package com.example.databindingproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;
import com.example.databindingproject.databinding.ActivityMainBinding;


public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding b;

    private Pojo thePojo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.thePojo = new Pojo();
        this.b = DataBindingUtil.setContentView(this, R.layout.activity_main);
        b.setMonBeauPojo(this.thePojo);
    }

    public void onClickButton(View view) {
        b.setMaBelleChaine("Toto");
        b.setMonEntier(12);

        this.thePojo.setPremierAttribut("Nicolas");
        this.thePojo.setSecondeAttribut(125);
    }
}