package com.example.databindingproject;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class Pojo extends BaseObservable {

    private String premierAttribut;

    private int secondeAttribut;

    public Pojo(String premierAttribut, int secondeAttribut) {
        this.premierAttribut = premierAttribut;
        this.secondeAttribut = secondeAttribut;
    }

    public Pojo() {
        this("Default", 15);
    }

    @Bindable
    public String getPremierAttribut() {
        return premierAttribut;
    }

    public void setPremierAttribut(String premierAttribut) {
        this.premierAttribut = premierAttribut;
        notifyPropertyChanged(BR.premierAttribut);
    }

    @Bindable
    public int getSecondeAttribut() {
        return secondeAttribut;
    }

    public void setSecondeAttribut(int secondeAttribut) {
        this.secondeAttribut = secondeAttribut;
        notifyPropertyChanged(BR.secondeAttribut);
    }
}
